import React from 'react';
import ReactDOM from 'react-dom';
import WeddingPage from './components/wedding-page';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import WeddingExpensesPage from './components/wedding-expense-page';
import ExpensePage from './components/expense-page';
import LoginPage from './components/login-page';
import ChatPage from './components/chat-page';
import FileUploadPage from './components/file-upload-page';


ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Route exact path="/">
        <Redirect to="/login" />
      </Route>
      <Route path="/login">
        <LoginPage></LoginPage>
      </Route>
      <Route path="/wedding">
        <WeddingPage></WeddingPage>
      </Route>
      <Route path="/wedding-expenses/:weddingId">
        <WeddingExpensesPage></WeddingExpensesPage>
      </Route>
      <Route path="/expenses">
        <ExpensePage></ExpensePage>
      </Route>
      <Route path="/chat">
        <ChatPage></ChatPage>
      </Route>
      <Route path ="/file-upload">
        <FileUploadPage/>
      </Route>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);


