import { useState, useRef } from "react";
import axios from  "axios";


export default function SingleWedding(){

    const weddingIdInput = useRef(null);
    const [wedding, setWedding] = useState([]);
    const [showWedding, setShowWedding] = useState(false);

    const clickToShow = () =>{
        if(showWedding){
            setShowWedding(false);
        }
        else{
            setShowWedding(true);
            getSpecificWedding();
        }
    }

    async function getSpecificWedding(){
        const response = await axios.get(`http://35.192.1.56:5432/weddings/${weddingIdInput.current.value}`);
        setWedding(response.data)
    }

    return(
        <div>
            <input placeholder="Wedding Id" ref={weddingIdInput}  onChange ={() => setShowWedding(false)}></input>
            <button onClick={clickToShow}>Get specific wedding</button>
            {showWedding ?
            <table>
                <thead><tr><th>ID</th><th>Date</th><th>Location</th><th>Name</th><th>Budget</th></tr></thead>
                <tbody>
                    <tr><td>{wedding.weddingId}</td><td>{wedding.date}</td><td>{wedding.location}</td><td>{wedding.name}</td><td>{wedding.budget}</td></tr>
                </tbody>
            </table>
            : null}
        </div>
    )

}