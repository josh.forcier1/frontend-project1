import Popover from 'react-bootstrap/Popover'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Button from 'react-bootstrap/Button'
import { useRef } from 'react';
import axios from 'axios';


export default function ExpenseTable(props){
    const [expenses, setExpenses] = props.functions;
    const specific = props.specific;

    const reasonInput = useRef(null);
    const amountInput = useRef(null);

    const popover = (exp) =>{
        const expense = exp;

        return(
            <Popover id="popover-basic">
            <Popover.Header as="h3">Enter New Information</Popover.Header>
            <Popover.Body>
                <table>
                    <thead><tr><th></th><th></th></tr></thead>
                    <tbody>
                        <tr><td>Reason:<br></br></td><td> <input placeholder="reason" defaultValue={expense.reason} ref={reasonInput}></input></td></tr>
                        <tr><td>Amount:<br></br></td><td><input placeholder="amount" defaultValue={expense.amount} ref={amountInput}></input></td></tr>
                    </tbody>
                </table>
                <button onClick={()=>editExpense(expense)}>Update Expense</button>
            </Popover.Body>
          </Popover>
        )
    }
    
      
      
    const Edit = (props) => {
        const expense = props.expense;
        return(
            <OverlayTrigger trigger="click" placement="right" overlay={popover(expense)}>
            <Button variant="success">Edit</Button>
          </OverlayTrigger>

        )};

    
    async function editExpense(expense){
        const expenseNew = {
            expenseId:0,
            reason:reasonInput.current.value,
            amount:amountInput.current.value,
            foreignWeddingId:0
        }
        await axios.put(`http://35.192.1.56:5432/expenses/${expense.expenseId}`, expenseNew)
        if (specific){
            const response = await axios.get(`http://35.192.1.56:5432/weddings/${expense.foreignWeddingId}/expenses`)
            setExpenses(response.data);
        }
        else{
            const response = await axios.get(`http://35.192.1.56:5432/expenses`)
            setExpenses(response.data); 
        }
        alert("You updated an expense")
    }

    async function deleteExpense(expense){
        await axios.delete(`http://35.192.1.56:5432/expenses/${expense.expenseId}`)
 
        if (specific){
            const response = await axios.get(`http://35.192.1.56:5432/weddings/${expense.foreignWeddingId}/expenses`)
            setExpenses(response.data);
        }
        else{
            const response = await axios.get(`http://35.192.1.56:5432/expenses`)
            setExpenses(response.data); 
        }
        alert("You deleted an expense")

    }


    return(
        <table>
            <thead><tr><th>ID</th><th>reason</th><th>amount</th><th>Wedding Id</th></tr></thead>
            <tbody>
                {expenses.sort((a, b) => (a.expenseId > b.expenseId) ? 1 : -1).map(e => <tr key={e.expenseId}>
                    <td>{e.expenseId}</td>
                    <td>{e.reason}</td>
                    <td>{e.amount}</td>
                    <td>{e.foreignWeddingId}</td>
                    <td><Edit expense={e}/></td>
                    <td><button onClick={() =>deleteExpense(e)}>Delete</button></td>
                </tr>)}
            </tbody>
        </table>)

}