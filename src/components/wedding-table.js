import Popover from 'react-bootstrap/Popover'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Button from 'react-bootstrap/Button'
import { useRef } from 'react';
import axios from 'axios';
import { Link } from "react-router-dom";


export default function WeddingTable(props){

    const [weddings, setWeddings] = props.functions;


    const dateInput = useRef(null);
    const locationInput = useRef(null);
    const nameInput = useRef(null);
    const budgetInput = useRef(null)

    const popover = (wed) =>{
        const wedding = wed;

        return(
            <Popover id="popover-basic">
            <Popover.Header as="h3">Enter New Information</Popover.Header>
            <Popover.Body>
                <table>
                    <thead><tr><th></th><th></th></tr></thead>
                    <tbody>
                        <tr><td>Date:<br></br></td><td> <input placeholder="date" defaultValue={wedding.date} ref={dateInput}></input></td></tr>
                        <tr><td>Location:<br></br></td><td><input placeholder="location" defaultValue={wedding.location} ref={locationInput}></input></td></tr>
                        <tr><td>Name:</td><td><input placeholder="name" defaultValue={wedding.name} ref={nameInput}></input></td></tr>
                        <tr><td>Budget:</td><td><input placeholder="budget" defaultValue={wedding.budget} ref={budgetInput}></input></td></tr>
                    </tbody>
                </table>
                <button onClick={()=>editWedding(wedding.weddingId)}>Update Wedding</button>
            </Popover.Body>
          </Popover>
        )
    }
    
      
      
    const Edit = (props) => {
        const wedding = props.wedding;
        return(
            <OverlayTrigger trigger="click" placement="right" overlay={popover(wedding)}>
            <Button variant="success">Edit</Button>
          </OverlayTrigger>

        )};

    
    async function editWedding(weddingId){
        const wedding = {
            weddingId:0,
            date:dateInput.current.value,
            location:locationInput.current.value,
            name:nameInput.current.value,
            budget:budgetInput.current.value
        }
        await axios.put(`http://35.192.1.56:5432/weddings/${weddingId}`, wedding)
        const response = await axios.get('http://35.192.1.56:5432/weddings')
        setWeddings(response.data);
        alert("You updated a wedding")
    }

    async function deleteWedding(weddingId){
        await axios.delete(`http://35.192.1.56:5432/weddings/${weddingId}`)
 
        const response = await axios.get('http://35.192.1.56:5432/weddings')
        setWeddings(response.data);
        alert("You deleted a wedding")

    }



    return(
        <table>
            <thead><tr><th>ID</th><th>Date</th><th>Location</th><th>Name</th><th>Budget</th></tr></thead>
            <tbody>
                {weddings.sort((a, b) => (a.weddingId > b.weddingId) ? 1 : -1).map(w => <tr key={w.weddingId}>
                    <td>{w.weddingId}</td>
                    <td>{w.date}</td>
                    <td>{w.location}</td>
                    <td>{w.name}</td>
                    <td>{w.budget}</td>
                    <td><Edit wedding={w} ></Edit></td>
                    <td><button onClick={() => deleteWedding(w.weddingId)}>Delete</button></td>
                    {/* <td><ExpenseForm weddingId={w.weddingId}></ExpenseForm></td> */}
                    <td><Link to={`/wedding-expenses/${w.weddingId}`}><button>Expenses</button></Link></td>
                </tr>)}
            </tbody>
        </table>)
}