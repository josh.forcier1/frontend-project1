import { useState, useRef } from "react";
import axios from  "axios";
import NavBar from "./navbar";
import ExpenseTable from "./expense-table";
import SingleExpense from "./single-expense";
import React from 'react'


export default function ExpensePage(){

    const [expenses, setExpenses] = useState([]);
    const [weddingExpenses, setWeddingExpenses] = useState([]);
    const weddingIdInput = useRef();
    const [showAllTable, setShowAllTable] = useState(false);
    const [showSpecificTable, setShowSpecificTable] = useState(false);

    const clickToShowAll = () => {
        if(showAllTable){
            setShowAllTable(false);
        }
        else{
            setShowAllTable(true);
            getAllExpenses(); 
        }
    }

    const clickToShowSpecific = () =>{
        if(showSpecificTable){
            setShowSpecificTable(false);
        }
        else{
            setShowSpecificTable(true);
            getSpecificWeddingExpenses();
        }
    }


    async function getAllExpenses(event){
        const response = await axios.get('http://35.192.1.56:5432/expenses')
        console.log(response.data);
        setExpenses(response.data);
    }

    async function getSpecificWeddingExpenses(){
        console.log(weddingIdInput.current.value);
        const response = await axios.get(`http://35.192.1.56:5432/weddings/${weddingIdInput.current.value}/expenses`);
        setWeddingExpenses(response.data)
        console.log(response.data)
    }



    return(
        <div>
            <NavBar/>
            <h1>Expense Page</h1>
            <button onClick={clickToShowAll}>Get all expenses</button>
            {showAllTable ? <ExpenseTable specific ={false} functions={[expenses,setExpenses]}/> : null}
            <input placeholder="Wedding Id" ref={weddingIdInput} onChange ={() => setShowSpecificTable(false)}></input>
            <button onClick={clickToShowSpecific}>Get Expenses of Wedding</button>
            {showSpecificTable ? <ExpenseTable specific ={true} functions={[weddingExpenses, setWeddingExpenses]}></ExpenseTable> : null}
            <SingleExpense/>
            {/* <SingleWedding/>
            <WeddingForm></WeddingForm>
            <WeddingExpenses></WeddingExpenses> */}
        </div>
    )

}