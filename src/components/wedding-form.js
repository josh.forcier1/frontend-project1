import { useRef } from 'react';
import axios from 'axios';

export default function WeddingForm(props){
    const [setWeddings] = props.functions;
    const dateInput = useRef(null);
    const locationInput = useRef(null);
    const nameInput = useRef(null);
    const budgetInput = useRef(null)

    async function addWedding(){
        const wedding = {
            weddingId:0,
            date:dateInput.current.value,
            location:locationInput.current.value,
            name:nameInput.current.value,
            budget:budgetInput.current.value
        }
        await axios.post(`http://35.192.1.56:5432/weddings`, wedding)
        const response = await axios.get('http://35.192.1.56:5432/weddings')
        setWeddings(response.data);
        alert("New wedding created")


    }
    return(
        <div>
            <h3>Wedding Form</h3>
            <input placeholder="date" ref={dateInput}></input>
            <input placeholder="location" ref={locationInput}></input>
            <input placeholder="name" ref={nameInput}></input>
            <input placeholder="budget" ref={budgetInput}></input>
            <button onClick={addWedding}>Add Wedding</button>
        </div>
    )
}