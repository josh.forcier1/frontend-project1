import { useRef } from 'react';
import axios from 'axios';
import Popover from 'react-bootstrap/Popover'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Button from 'react-bootstrap/Button'


export default function ExpenseForm(props){
    const weddingId = props.weddingId;
    const [setExpenses] = props.functions;
    const reasonInput = useRef(null);
    const amountInput = useRef(null);



    const popover = () =>{

        return(
            <Popover id="popover-basic">
            <Popover.Header as="h3">Enter New Information</Popover.Header>
            <Popover.Body>
                <table>
                    <thead><tr><th></th><th></th></tr></thead>
                    <tbody>
                        <tr><td>Reason:<br></br></td><td> <input placeholder="reason" ref={reasonInput}></input></td></tr>
                        <tr><td>Location:<br></br></td><td><input placeholder="amount" ref={amountInput}></input></td></tr>
                    </tbody>
                </table>
                <button onClick={()=>addExpense(weddingId)}>Add Expense</button>
            </Popover.Body>
          </Popover>
        )
    }
    
      
      
    const Add = () => {
        return(
            <OverlayTrigger trigger="click" placement="right" overlay={popover()}>
            <Button variant="success">Add Expense</Button>
          </OverlayTrigger>

        )};



    async function addExpense(){
        const expense = {
            expenseId:0,
            reason:reasonInput.current.value,
            amount:amountInput.current.value,
            foreignWeddingId:0
        }
        await axios.post(`http://35.192.1.56:5432/weddings/${weddingId}/expenses`, expense)
        alert("New expense created")
        const response = await axios.get(`http://35.192.1.56:5432/weddings/${weddingId}/expenses`);
        setExpenses(response.data)

    }
    return(
        <Add></Add>
    )
}