import { useState, useRef } from "react";
import axios from  "axios";


export default function SingleExpense(){

    const expenseIdInput = useRef(null);
    const [expense, setExpense] = useState([]);
    const [showExpense,setShowExpense] = useState(false)

    async function getSpecificExpense(){
        const response = await axios.get(`http://35.192.1.56:5432/expenses/${expenseIdInput.current.value}`);
        setExpense(response.data)
        console.log(response)
    }

    const clickToShow = () =>{
        if(showExpense){
            setShowExpense(false);
        }
        else{
            setShowExpense(true);
            getSpecificExpense();
        }
    }

    return(
        <div>
            <input placeholder="Expense Id" ref={expenseIdInput} onChange ={() => setShowExpense(false)}></input>
            <button onClick={clickToShow}>Get specific expense</button>
            {showExpense ? <table>
                <thead><tr><th>ID</th><th>Reason</th><th>Amount</th><th>Wedding ID</th></tr></thead>
                <tbody>
                    <tr><td>{expense.expenseId}</td><td>{expense.reason}</td><td>{expense.amount}</td><td>{expense.foreignWeddingId}</td></tr>
                </tbody>
            </table> : null}
        </div>
    )

}