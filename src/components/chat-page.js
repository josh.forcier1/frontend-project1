import NavBar from "./navbar";
import { useEffect, useRef, useState } from "react";
import axios from  "axios";

export default function ChatPage(){

    const messageInput = useRef();
    const emailInput = useRef();
    const [messages, setMessages] = useState([]);
    const [showMessages,setShowMessages] = useState(false);

    useEffect(() => {
        async function getMessages(){
            if(showMessages){
                try {
                    const response = await axios.get(`https://messaging-service-dot-wedding-planner-project1.uc.r.appspot.com/messages`, { params: { sender: localStorage.getItem('email'), recipient: emailInput.current.value }});
                    //setMessages(response.data)
                    const response2 = await axios.get(`https://messaging-service-dot-wedding-planner-project1.uc.r.appspot.com/messages`, { params: { sender: emailInput.current.value, recipient: localStorage.getItem('email') }});
                    setMessages(response.data.concat(response2.data))
                } catch (error) {
                    const response = await axios.get(`https://messaging-service-dot-wedding-planner-project1.uc.r.appspot.com/messages`, { params: { sender: localStorage.getItem('email'), recipient: emailInput.current.value }});
                    setMessages(response.data)
                }
            }
        }
        getMessages();
    })

    async function sendMessage(){
        try {
            await axios.get(`https://authorization-service-dot-wedding-planner-project1.uc.r.appspot.com/users/${emailInput.current.value}/verify`)
            await axios.get(`https://authorization-service-dot-wedding-planner-project1.uc.r.appspot.com/users/${localStorage.getItem('email')}/verify`)
            await axios.post(`https://messaging-service-dot-wedding-planner-project1.uc.r.appspot.com/messages`, {sender:localStorage.getItem('email'),recipient:emailInput.current.value,note:messageInput.current.value,timestamp:Date.now()})
            alert("Message Sent");
            setShowMessages(true);
        } catch (error) {
            alert("Message failed, check that the recipient is valid")
        }
    }

    function verifyReciever(m){
        if(m.sender !== localStorage.getItem('email')){
            return(
                <p>{emailInput.current.value}: {m.note}</p>
            )
        }
    }

    function verifySender(m){
        if(m.sender === localStorage.getItem('email')){
            return(
                <p>{m.note}     :{localStorage.getItem('email')}</p>
            )
        }
    }

    return(
        <div>
            <NavBar></NavBar>
            <label for="1">Recipient</label>
            <input id="1" placeholder="example@example.com" ref={emailInput} />
            <p>message:</p>
            <input placeholder="Enter message here" ref= {messageInput}></input>
            <button onClick={() => sendMessage()}>Send Message</button>
            <table>
            <thead><tr><th>Recipient</th><th>Sender</th></tr></thead>
            <tbody>
                {messages.sort((a, b) => (a.timestamp > b.timestamp) ? 1 : -1).map(m => <tr key={m.messageId}>
                    <td>{verifyReciever(m)}</td>
                    <td>{verifySender(m)}</td>
                </tr>)}
            </tbody>
            </table>
        </div>
    )
}