import { useRef, useState } from "react";
import axios from  "axios";
import ExpenseTable from "./expense-table";

export default function WeddingExpenses(props){
    //const weddingId = props.weddingId;
    const weddingIdInput = useRef(null);
    const [expenses,setExpenses] = useState([]);
    const [allExpenses, setAllExpenses] = useState([]);

    async function getSpecificWeddingExpenses(){
        console.log(weddingIdInput.current.value);
        const response = await axios.get(`http://35.192.1.56:5432/weddings/${weddingIdInput.current.value}/expenses`);
        setExpenses(response.data)
        console.log(response.data)
    }

    async function getAllExpenses(){
        const response = await axios.get(`http://35.192.1.56:5432/expenses`);
        setAllExpenses(response.data);
    }

    return(
        <div>
            <input placeholder="Wedding Id" ref={weddingIdInput}></input>
            <button onClick={() => getSpecificWeddingExpenses()}>Get Expenses of Wedding</button>
            <ExpenseTable functions={[expenses,setExpenses]}></ExpenseTable>
            <button onClick={() => getAllExpenses()}>Get all expenses</button>;
            <ExpenseTable functions={[allExpenses,setAllExpenses]}></ExpenseTable>
        </div>
    )
}