import axios from "axios";
import { useRef, useState } from "react";
import NavBar from "./navbar";



export default function FileUploadPage(){

    const nameInput = useRef();
    const [selectedFiles, setSelectedFiles] = useState(undefined)
    const [imageLink,setImageLink] = useState();
    

    const selectFile = (event) => {
        setSelectedFiles(event.target.files);
      };

      function getBase64(file) {
        return new Promise((resolve, reject) => {
          const reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = () => {
            let encoded = reader.result.toString().replace(/^data:(.*,)?/, '');
            if ((encoded.length % 4) > 0) {
              encoded += '='.repeat(4 - (encoded.length % 4));
            }
            resolve(encoded);
          };
          reader.onerror = error => reject(error);
        });
      }

    async function uploadFile(){;

        const fileContent = await getBase64(selectedFiles[0])
        const fileExtension = (/[.]/.exec(selectedFiles[0].name)) ? /[^.]+$/.exec(selectedFiles[0].name) : undefined;
        const response = await axios.post('https://us-central1-wedding-planner-project1.cloudfunctions.net/file-upload', {name:nameInput.current.value, extension:fileExtension, content: fileContent})
        setImageLink(response.data.photoLink)
        //console.log(response.data.photoLink)
        //console.log(response.data[0])
        //console.log("test")
        // console.log("reaches")
        // console.log(fileContent)
    }

    return(
        <div>
            <NavBar/>
            <input type="file" onChange={selectFile} />
            <input placeholder="name" ref={nameInput}/>
            <button onClick={() => uploadFile()}>Upload</button>
            <p><a href={imageLink} target="_blank" rel="noreferrer">{imageLink}</a></p>
        </div>
    )
}