import { useParams } from "react-router-dom";
import {useState, useEffect } from "react";
import ExpenseTable from "./expense-table";
import axios from "axios";
import { Link } from "react-router-dom";
import NavBar from "./navbar";
import ExpenseForm from "./expense-form";

export default function WeddingExpensesPage(){
    
    const {weddingId} = useParams();
    const [expenses,setExpenses] = useState([]);

    useEffect(() => {
        async function getSpecificWeddingExpenses(){
            const response = await axios.get(`http://35.192.1.56:5432/weddings/${weddingId}/expenses`);
            setExpenses(response.data)
            console.log(response.data)
        }
        getSpecificWeddingExpenses();
    }, [weddingId])

    return(
        <div>
            <NavBar/>
            <ExpenseTable specific={true} functions={[expenses,setExpenses]}></ExpenseTable>
            <td><ExpenseForm functions={[expenses, setExpenses]} weddingId={weddingId}></ExpenseForm></td>
            <Link to={`/wedding`}><button>Back</button></Link>
        </div>
    )
}