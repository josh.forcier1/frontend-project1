import { useRef } from 'react';
import axios from 'axios';
import { useHistory } from "react-router-dom";

export default function LoginPage(){

    const emailInput = useRef();
    const passwordInput = useRef();
    const emailInput2 = useRef();
    const history = useHistory();

    async function login(){
        try {
            console.log(emailInput.current.value);
            const response = await axios.patch(`https://authorization-service-dot-wedding-planner-project1.uc.r.appspot.com/users/login`, {email:emailInput.current.value, password:passwordInput.current.value})
            console.log(response);
            alert("Logged in successfully");
            localStorage.setItem('email', emailInput.current.value);
            localStorage.setItem('fname', response.data.fname);
            localStorage.setItem('lname', response.data.lname);
            history.push("/wedding");
        } catch (error) {
            alert("Incorrect username or password please try again")
        }
    }

    async function verify(){
        try {
            await axios.get(`https://authorization-service-dot-wedding-planner-project1.uc.r.appspot.com/users/${emailInput2.current.value}/verify`)
            alert("Email verified");
        } catch (error) {
            alert("Email didn't verify");
        }

    }
    return(
        <div>
            <input placeholder="email" ref={emailInput}></input>
            <input type="password" placeholder="password" ref={passwordInput}></input>
            <button onClick={login}>Login</button>
            <input placeholder="email" ref={emailInput2}></input>
            <button onClick={verify}>Verify</button>

        </div>
    )
}