import Popover from 'react-bootstrap/Popover'
 
import { useRef } from 'react';
import axios from 'axios';

export default function PopoverFun(props){

    const weddingId = props.weddingId

    const dateInput = useRef(null);
    const locationInput = useRef(null);
    const nameInput = useRef(null);
    const budgetInput = useRef(null)

    async function editWedding(weddingId){
        console.log(weddingId);
        const wedding = {
            weddingId:0,
            date:dateInput.current.value,
            location:locationInput.current.value,
            name:nameInput.current.value,
            budget:budgetInput.current.value
        }
        await axios.put(`http://localhost:5432/weddings/${weddingId}`, wedding)
        alert("You updated a wedding")
    }



    return(
        <Popover id="popover-basic">
        <Popover.Header as="h3">Enter New Information</Popover.Header>
        <Popover.Body>
            <input placeholder="date" ref={dateInput}></input>
            <input placeholder="location" ref={locationInput}></input>
            <input placeholder="name" ref={nameInput}></input>
            <input placeholder="budget" ref={budgetInput}></input>
            <button onClick={()=>editWedding(weddingId)}>Update Wedding</button>
        </Popover.Body>
      </Popover>
    )
}