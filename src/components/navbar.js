import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'

export default function NavBar(){
    return(
        <Navbar bg="dark" variant="dark">

                <Navbar.Brand href="/">Wedding Service</Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify-content-end">
                <Nav className="me-auto">\
                    <Nav.Link href="/wedding">Weddings</Nav.Link>
                    <Nav.Link href="/expenses">Expenses</Nav.Link>
                    <Nav.Link href="/chat">Chat</Nav.Link>
                    <Nav.Link href="/file-upload">File Upload</Nav.Link>
                </Nav>
                <Navbar.Text>
                    Signed in as: <a href="#login">{`${localStorage.getItem('fname')} ${localStorage.getItem('lname')}`}</a>
                </Navbar.Text>
                </Navbar.Collapse>

        </Navbar>
    )
}
