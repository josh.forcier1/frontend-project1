import { useState } from "react";
import axios from  "axios";
import WeddingTable from "./wedding-table";
import SingleWedding from "./single-wedding";
import WeddingForm from "./wedding-form";
import NavBar from "./navbar";



export default function WeddingPage(){


    const [weddings, setWeddings] = useState([]);
    const [showWeddings,setShowWeddings] = useState(false);

    const clickToShowAll = () => {
        if(showWeddings){
            setShowWeddings(false);
        }
        else{
            setShowWeddings(true);
            getWeddings(); 
        }
    }

    async function getWeddings(event){
        const response = await axios.get('http://35.192.1.56:5432/weddings')
        console.log(response.data);
        setWeddings(response.data);
    }



    return(
        <div>
            <NavBar/>
            <h1>Wedding Page</h1>
            <button onClick={clickToShowAll}>Get Weddings</button>
            {showWeddings ? <WeddingTable functions={[weddings,setWeddings]}></WeddingTable> : null}
            <SingleWedding/>
            <WeddingForm functions={[weddings,setWeddings]}></WeddingForm>
            {/* <WeddingExpenses></WeddingExpenses> */}
        </div>
    )
}